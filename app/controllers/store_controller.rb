class StoreController < ApplicationController
	def index
		if session[:counter].nil?
			session[:counter] = 1
		else
			session[:counter] += 1
		end

		@visits = session[:counter]

		@products = Product.all
		@cart = current_cart
	end
end
